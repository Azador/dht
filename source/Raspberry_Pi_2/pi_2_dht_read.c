// Copyright (c) 2014 Adafruit Industries
// Author: Tony DiCola

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

//#define SHOW_DEBUGS

#ifdef SHOW_DEBUGS
#include <stdio.h>
#endif

#include "pi_2_dht_read.h"
#include "pi_2_mmio.h"

// This is the only processor specific magic value, the maximum amount of time to
// spin in a loop before bailing out and considering the read a timeout.  This should
// be a high value, but if you're running on a much faster platform than a Raspberry
// Pi or Beaglebone Black then it might need to be increased.
#define DHT_MAXCOUNT 32000

// Number of bit pulses to expect from the DHT.  Note that this is 41 because
// the first pulse is a constant 50 microsecond pulse, with 40 pulses to represent
// the data afterwards.
#define DHT_PULSES 41

int find_highest_diff (int high[], int low1[], int low2[])
{
  int abs_max = 0;
  int max_pos = 0;
  
  for (int i=0; i < DHT_PULSES; ++i)
    {
      if (abs (high[i]) > abs_max)
	{
	  abs_max = abs (high[i]);
	  max_pos = i;
	}

      if (abs (low1[i]) < abs (low2[i]))
	{
	  if (abs (low1[i]) > abs_max)
	    {
	      abs_max = abs (low1[i]);
	      max_pos = i;
	    }
	}
      else
	{
	  if (abs (low2[i]) > abs_max)
	    {
	      abs_max = abs (low2[i]);
	      max_pos = i;
	    }
	}
    }

  return max_pos;
}

bool need_adjust (int pos, int high[], int low1[], int low2[])
{
  if (abs (high[pos]) >= 10)
    return true;

  if (abs (low1[pos]) < abs (low2[pos]))
    return abs (low1[pos]) >= 10;

  return abs (low2[pos]) >= 10;
}

bool adjust (int pos, int uSeconds[], int high[], int low1[], int low2[])
{
  bool use_high = true;
  
  if (abs (low1[pos]) < abs (low2[pos]))
    {
      if (abs (high[pos]) < abs (low1[pos]))
	use_high = false;
    }
  else
    {
      if (abs (high[pos]) < abs (low2[pos]))
	use_high = false;
    }

  if (use_high && pos > 0)
    {
      int d = 0;

      if (abs (low1[pos-1]) < abs (low2[pos-1]))
	d = low1[pos-1];
      else
	d = low2[pos-1];

      if (high[pos]*d > 0)
	{
	  low1[pos-1] -= d;
	  low2[pos-1] -= d;
	  high[pos] -= d;
	  uSeconds[pos*2-1] += d;
	  uSeconds[pos*2] -= d;
	  return true;
	}
    }
  
  if (use_high)
    {
      int d = 0;

      if (abs (low1[pos]) < abs (low2[pos]))
	d = low1[pos];
      else
	d = low2[pos];

      if (high[pos]*d > 0)
	{
	  low1[pos] -= d;
	  low2[pos] -= d;
	  high[pos] -= d;
	  uSeconds[pos*2+1] += d;
	  uSeconds[pos*2] -= d;
	  return true;
	}
    }
  else
    {
      int d = high[pos];
      int l = 0;
      if (abs (low1[pos]) < abs (low2[pos]))
	l = low1[pos];
      else
	l = low2[pos];
      
      if (l*d > 0)
	{
	  low1[pos] -= d;
	  low2[pos] -= d;
	  high[pos] -= d;
	  uSeconds[pos*2+1] += d;
	  uSeconds[pos*2] -= d;
	  return true;
	}
    }

  if (!use_high && pos < DHT_PULSES-1)
    {
      int d = high[pos+1];
      int l = 0;
      if (abs (low1[pos]) < abs (low2[pos]))
	l = low1[pos];
      else
	l = low2[pos];
      
      if (l*d > 0)
	{
	  low1[pos] -= d;
	  low2[pos] -= d;
	  high[pos+1] -= d;
	  uSeconds[pos*2+1] += d;
	  uSeconds[pos*2+2] -= d;
	  return true;
	}
    }
  
  return false;
}

void adjust_reads (int uSeconds[DHT_PULSES*2])
{
  int high[DHT_PULSES];
  int low1[DHT_PULSES];
  int low2[DHT_PULSES];
  
  high[0] = uSeconds[0]-80;
  low1[0] = 80-uSeconds[1];
  low2[0] = low1[0];
  
  for (int i=2; i < DHT_PULSES*2; i+=2)
    {
      high[i/2] = uSeconds[i]-50;
      low1[i/2] = 28-uSeconds[i+1];
      low2[i/2] = 70-uSeconds[i+1];
    }

  int bit0[4];
  bit0[0] = high[9];
  bit0[1] = high[17];
  bit0[2] = high[25];
  bit0[3] = high[33];
  for (int i=3; i > 0; --i)
    for (int j=0; j < i; ++j)
      if (bit0[j] > bit0[j+1])
	{
	  int tmp = bit0[j];
	  bit0[j] = bit0[j+1];
	  bit0[j+1] = tmp;
	}
  
  int bit0_high_correction = (bit0[1] + bit0[2]) / 2;
  high[9] -= bit0_high_correction;
  high[17] -= bit0_high_correction;
  high[25] -= bit0_high_correction;
  high[33] -= bit0_high_correction;
  uSeconds[18] -= bit0_high_correction;
  uSeconds[34] -= bit0_high_correction;
  uSeconds[50] -= bit0_high_correction;
  uSeconds[66] -= bit0_high_correction;

#ifdef SHOW_DEBUGS
  printf (        "    Low  High  Wert  Low-Diff  High-Diff1 High-Diff2\n");
  for (int i=0; i < DHT_PULSES*2; i+=2)
    {
      printf ("%2i:  %2i   %2i    %i     %3i       %3i        %3i\n", i,
	      uSeconds[i], uSeconds[i+1],
  	      uSeconds[i+1] >= 50 ? 1 : 0,
	      high[i/2], low1[i/2], low2[i/2]);
    }
#endif

  for (;;)
    {
      int pos = find_highest_diff (high, low1, low2);
      
      if (!need_adjust (pos, high, low1, low2))
	break;

#ifdef SHOW_DEBUGS
      printf ("Change pos %i\n", pos);
#endif
      
      if (!adjust (pos, uSeconds, high, low1, low2))
	break;
  
#ifdef SHOW_DEBUGS
      printf ("    Low  High  Wert  Low-Diff  High-Diff1 High-Diff2\n");
      for (int i=0; i < DHT_PULSES*2; i+=2)
	{
	  printf ("%2i:  %2i   %2i    %i     %3i       %3i        %3i\n", i,
		  uSeconds[i], uSeconds[i+1],
		  uSeconds[i+1] >= 50 ? 1 : 0,
		  high[i/2], low1[i/2], low2[i/2]);
	}
#endif
    }
}

int pi_2_dht_read(int type, int pin, float* humidity, float* temperature) {
#ifdef SHOW_DEBUGS
  printf ("Debug output activated.\n");
#endif
  
  // Validate humidity and temperature arguments and set them to zero.
  if (humidity == NULL || temperature == NULL) {
    return DHT_ERROR_ARGUMENT;
  }
  *temperature = 0.0f;
  *humidity = 0.0f;

  // Initialize GPIO library.
  if (pi_2_mmio_init() < 0) {
#ifdef SHOW_DEBUGS
    printf ("Error GPIO.\n");
#endif
    return DHT_ERROR_GPIO;
  }

  // Store the count that each DHT bit pulse is low and high.
  // Make sure array is initialized to start at zero.
  int uSeconds[DHT_PULSES*2] = {0};

  // Set pin to output.
  pi_2_mmio_set_output(pin);

  // Bump up process priority and change scheduler to try to try to make process more 'real time'.
  set_max_priority();

  // Set pin high for ~500 milliseconds.
  pi_2_mmio_set_high(pin);
  sleep_milliseconds(500);

  // The next calls are timing critical and care should be taken
  // to ensure no unnecssary work is done below.

  // Set pin low for ~20 milliseconds.
  pi_2_mmio_set_low(pin);
  busy_wait_milliseconds(20);

  // Set pin at input.
  pi_2_mmio_set_input(pin);

  // Need a very short delay before reading pins or else value is sometimes still low.
  struct timespec last_timestamp, actual_timestamp;
  if (clock_gettime (CLOCK_MONOTONIC, &last_timestamp))
    {
#ifdef SHOW_DEBUGS
      printf ("Error clock_gettime start.\n");
#endif
      return DHT_ERROR_TIMEOUT;
    }
  
  unsigned int us = 0;
  for (;;) {
    if (clock_gettime (CLOCK_MONOTONIC, &actual_timestamp))
      {
#ifdef SHOW_DEBUGS
	printf ("Error clock_gettime wait.\n");
#endif
	return DHT_ERROR_TIMEOUT;
      }

    us =
      ((unsigned int)(actual_timestamp.tv_nsec - last_timestamp.tv_nsec)) / 1000;
    if (us > 10)
      break;
  }
  
  // Wait for DHT to pull pin low.
  uint32_t count = 0;
  while (pi_2_mmio_input(pin)) {
    if (++count >= DHT_MAXCOUNT) {
      // Timeout waiting for response.
      set_default_priority();
#ifdef SHOW_DEBUGS
      if (clock_gettime (CLOCK_MONOTONIC, &actual_timestamp))
	printf ("Error: Timeout waiting for first low signal after %d µs and %d counts.\n", us, count);
      else
	{
	  us =
	    ((unsigned int)(actual_timestamp.tv_nsec - last_timestamp.tv_nsec)) / 1000;
	  printf ("Error: Timeout waiting for first low signal after %d µs\n", us);
	}
#endif
      return DHT_ERROR_TIMEOUT;
    }
  }

  // Using the counter to recognize a timeout
  int counter = 0;

  // Get the initial time stamp
  if (clock_gettime (CLOCK_MONOTONIC, &last_timestamp))
    {
#ifdef SHOW_DEBUGS
      printf ("Error: clock_gettime start measurement.\n");
#endif
      return DHT_ERROR_TIMEOUT;
    }

  // Record pulse widths for the expected result bits.
  for (int i=0; i < DHT_PULSES*2; i+=2) {
    // Wait for the pin to get high again
    while (!pi_2_mmio_input(pin)) {
      if (++counter >= DHT_MAXCOUNT) {
        // Timeout waiting for response.
        set_default_priority();
#ifdef SHOW_DEBUGS
	printf ("Error: Timeout waiting for high pulse, i=%d.\n", i);
#endif
        return DHT_ERROR_TIMEOUT;
      }
    }

    // Get the next time stamp
    if (clock_gettime (CLOCK_MONOTONIC, &actual_timestamp))
      {
#ifdef SHOW_DEBUGS
	printf ("Error: clock_gettime measuring high pulse, i=%d.\n", i);
#endif
	return DHT_ERROR_TIMEOUT;
      }
    
    // Only use tv_nsec, if we have a second overflow, this will result in an underflow
    // in the computation of the difference...
    // Directly converting to microseconds.
    uSeconds[i] =
      ((unsigned int)(actual_timestamp.tv_nsec - last_timestamp.tv_nsec)) / 1000;
    last_timestamp.tv_nsec = actual_timestamp.tv_nsec;
    counter = 0;
    
    // Wait for the pin to get low again
    while (pi_2_mmio_input(pin)) {
      if (++counter >= DHT_MAXCOUNT) {
        // Timeout waiting for response.
        set_default_priority();
#ifdef SHOW_DEBUGS
	printf ("Error: Timeout waiting for low pulse, i=%d.\n", i);
#endif
        return DHT_ERROR_TIMEOUT;
      }
    }

    // Get the next time stamp
    if (clock_gettime (CLOCK_MONOTONIC, &actual_timestamp))
      {
#ifdef SHOW_DEBUGS
	printf ("Error: clock_gettime measuring low pulse, i=%d.\n", i);
#endif
	return DHT_ERROR_TIMEOUT;
      }
    
    // Only use tv_nsec, if we have a second overflow, this will result in an underflow
    // in the computation of the difference...
    // Directly converting to microseconds.
    uSeconds[i+1] =
      ((unsigned int)(actual_timestamp.tv_nsec - last_timestamp.tv_nsec)) / 1000;
    last_timestamp.tv_nsec = actual_timestamp.tv_nsec;
    counter = 0;
  }

  // Done with timing critical code, now interpret the results.

  // Drop back to normal priority.
  set_default_priority();

  adjust_reads (uSeconds);
  
  // The threshold is 50 microseconds
  int threshold = 50;

  // Interpret each high pulse as a 0 or 1 by comparing it to the 50us reference.
  // If the count is less than 50us it must be a ~28us 0 pulse, and if it's higher
  // then it must be a ~70us 1 pulse.
  uint8_t data[5] = {0};
  for (int i=3; i < DHT_PULSES*2; i+=2) {
    int index = (i-3)/16;
    data[index] <<= 1;
    if (uSeconds[i] >= threshold) {
      // One bit for long pulse.
      data[index] |= 1;
    }
    // Else zero bit for short pulse.
  }

  // Useful debug info:
#ifdef SHOW_DEBUGS
  printf("Data: 0x%x 0x%x 0x%x 0x%x 0x%x CS: 0x%x\n", data[0], data[1], data[2], data[3], data[4], 0xff & (data[0]+data[1]+data[2]+data[3]));
  printf("Temp: %4.1f  Hum: %4.1f\n",
  	 ((0x7f&data[2])*256+data[3])/10.0 * (0x80&data[0] ? -1 : 1),
  	 (data[0]*256+data[1])/10.0);
#endif

  // Verify checksum of received data.
  if (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) {
    if (type == DHT11) {
      // Get humidity and temp for DHT11 sensor.
      *humidity = (float)data[0];
      *temperature = (float)data[2];
    }
    else if (type == DHT22) {
      // Calculate humidity and temp for DHT22 sensor.
      *humidity = (data[0] * 256 + data[1]) / 10.0f;
      *temperature = ((data[2] & 0x7F) * 256 + data[3]) / 10.0f;
      if (data[2] & 0x80) {
        *temperature *= -1.0f;
      }
    }

    // Humidity is only valid between 0 and 100.
    // Operating range for the temperature is -40 - 80 °C for DHT22, DHT11 has an even
    // smaller operating range.
    // Allow values out of the operating range but reject insane values.
    if (*humidity >= 0.0 && *humidity <= 100.0
	&& *temperature >= -80.0 && *temperature <= 120.0)
      return DHT_SUCCESS;
    else
      return DHT_ERROR_CHECKSUM;
  }
  else {
    return DHT_ERROR_CHECKSUM;
  }
}
